import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LocalidadesService {

  constructor(private http: HttpClient) {
  }

  consultaEstados(): Observable<any> {
    return this.http.get('http://localhost:3000/estados').pipe(map((response: any) => {
      return response;
    }));
  }

  consultaMunicipiosPorUf(uf: string): Observable<any> {
    return this.http.get(`http://localhost:3000/municipios/${uf}`).pipe(map((response: any) => {
      return response;
    }));
  }

  consultaDistritoPorCodMunicipio(codMunicipio: number): Observable<any> {
    return this.http.get(`http://localhost:3000/distritos/${codMunicipio}`).pipe(map((response: any) => {
      return response;
    }));
  }
}
