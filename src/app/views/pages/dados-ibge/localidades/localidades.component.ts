import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {LocalidadesService} from './localidades.service';
import {DistritoModel} from './localidade.model';

@Component({
  selector: 'app-localidades',
  templateUrl: './localidades.component.html',
  styleUrls: ['./localidades.component.scss']
})
export class LocalidadesComponent implements OnInit {

  formGroup: FormGroup = new FormGroup({
    estadoUf: new FormControl(''),
    municipioId: new FormControl('')
  });
  estados: Array<any> = [];
  municipios: Array<any> = [];
  distritos: Array<DistritoModel> = [];
  constructor(
    private localidadeService: LocalidadesService
  ) { }

  ngOnInit(): void {
    this.loadEstados();
    this.formGroup.get('estadoUf').valueChanges.subscribe(value => {
      this.municipios = [];
      this.distritos = [];
      this.formGroup.get('municipioId').setValue('');
      this.loadMunicipios(value);
    });
    this.formGroup.get('municipioId').valueChanges.subscribe(value => {
      this.distritos = [];
      this.loadDistritos(value);
    });
  }

  loadEstados(): void {
    this.localidadeService.consultaEstados().subscribe(response => {
      this.estados = response;
    });
  }

  loadMunicipios(uf: string): void {
    this.localidadeService.consultaMunicipiosPorUf(uf).subscribe(response => {
      this.municipios = response;
    });
  }

  loadDistritos(municipioId: number): void {
    this.localidadeService.consultaDistritoPorCodMunicipio(municipioId).subscribe(response => {
      this.distritos = response;
    });
  }
}
