import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LocalidadesComponent } from './localidades/localidades.component';
import {ReactiveFormsModule} from '@angular/forms';
import {RouterModule, Routes} from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: LocalidadesComponent
  }
];

@NgModule({
  declarations: [LocalidadesComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)
  ]
})
export class DadosIbgeModule { }
